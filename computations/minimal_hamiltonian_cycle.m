function [ ordered_points ] = minimal_hamiltonian_cycle( points )
%MINIMAL_HAMILTONIAN_CYCLE Computes a Hamiltonian cycle with minimal
%Euclidean length from a set of curve-like points.
%   Detailed explanation goes here

distances = squareform(pdist(points));
% Compute the n closest points to each point to generate a reduced
%  set of candidate neighbors.
n_closest = 5;
[~, I] = cellfun(@(d) minn(d, n_closest+1), num2cell(distances, 1), ...
    'UniformOutput', false);
neighbors = false(size(distances));
for i = 1:numel(I)
    indices = I{i};
    neighbors(i, indices) = true;
end
neighbors = neighbors | neighbors'; % Symmetrize adjacency matrix.
reduced_distances = distances;
reduced_distances(~neighbors) = Inf;
% Compute the adjacency matrix for the minimal Hamiltonian cycle.
curve_adjacency = tsp_binary_programming(reduced_distances);
G = graph(curve_adjacency);
ordering = dfsearch(G, 1);
ordered_points = points(ordering, :);

end

