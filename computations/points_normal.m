function [ normal ] = points_normal( points )
%POINTS_NORMAL Computes the normal of a set of points
%   Detailed explanation goes here

% Subtract the centroid.
points = points - repmat(mean(points), size(points, 1), 1);
% Get the normal via singular value decomposition.
% See http://math.stackexchange.com/a/99317.
[~, ~, V] = svd(points);
normal = V(:, 3)';

end

