function [ cases ] = discover_cases( cases_path )
%DISCOVER_CASES Discovers possible cases in subfolders and discovers the 
%associated data files.
%   Detailed explanation goes here

CASE_PREFIX = 'Case';       % Case folder starts with this.
CASE_EXTENSION = '.csv';    % File extension of the data file.
CASE_PRE_POSTFIX = ['pre' CASE_EXTENSION];   % Preoperative data filename end.
CASE_POST_POSTFIX = ['post' CASE_EXTENSION]; % Postoperative data filename end.
cases_path = fullfile(pwd(), '..', 'data');

% List all candidate folders and check them.
folders = dir(fullfile(cases_path, '*'));
cases = {};
for i = 1:length(folders)
    folder = folders(i);
    % Check if the item is a folder and if its name starts with the prefix.
    if folder.isdir && ...
            strncmp(folder.name, CASE_PREFIX, length(CASE_PREFIX))
        % Case folder found.
        cases{end+1} = struct;
        cases{end}.foldername = folder.name;
        cases{end}.folderpath = fullfile(cases_path, folder.name);
        % Discover the data files.
        data_path = cases{end}.folderpath;
        filepath_pattern = fullfile(data_path, ['*' CASE_EXTENSION]);
        files = dir(filepath_pattern);
        for j = 1:length(files)
            file = files(j);
            % Match filename postfixes by reversing strings.
            if ~file.isdir && ...
                    strncmp(fliplr(file.name), fliplr(CASE_PRE_POSTFIX), ...
                    length(CASE_PRE_POSTFIX))
                cases{end}.filepath_pre = ...
                    GetFullPath(fullfile(data_path, file.name));
            elseif ~file.isdir && ...
                    strncmp(fliplr(file.name), fliplr(CASE_POST_POSTFIX), ...
                    length(CASE_POST_POSTFIX))
                cases{end}.filepath_post = ...
                    GetFullPath(fullfile(data_path, file.name));
            end
        end
    end
end

end

