function [ M, I ] = minn( A, n )
%MINN Smallest n elements in array
%   Detailed explanation goes here

% From http://stackoverflow.com/a/14140834
[sorted, indices] = sort(A);
M = sorted(1:n);
I = indices(1:n);

end

