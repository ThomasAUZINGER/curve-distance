function [ h ] = plot3m( points, varargin )
%PLOT3M Plot3 for array inputs.
%   Detailed explanation goes here

dim = find(size(points) ~= 3); % Get the split dimension.
temp = num2cell(points, dim);
[X, Y, Z] = temp{:}; % Extract columns.
h = plot3(X, Y, Z, varargin{:});

end

