function [ transform_origin, transform_registration, registered ] = ...
    centered_registration( moving, fixed, varargin )
%CENTERED_REGISTRATION Registeres two points clouds at the origin
%   Detailed explanation goes here

% Determine translation of moving point cloud to origin.
transform_origin = affine3d();
transform_origin.T(4, 1:3) = mean(moving.Location);
% Move point clouds to origin.
moving_centered = pctransform(moving, invert(transform_origin));
fixed_centered = pctransform(fixed, invert(transform_origin));
% Perform registration.
[transform_registration, registered_centered] = pcregrigid(...
    moving_centered, fixed_centered, varargin{:});
% Shift registered point cloud away from origin.
registered = pctransform(registered_centered, transform_origin);

end

