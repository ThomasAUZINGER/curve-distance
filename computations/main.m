%% Initialize.
clear all
close all
clc
plot3style = {'-o', 'MarkerFaceColor', 'r'};

%% Setup.
% Choose if the pre- and postoperative points should be rigidly registered
% to each other.
REGISTER_CURVES = false;

%% Discover cases.
cases_path = GetFullPath(fullfile(pwd(), '..', 'data'));
cases = discover_cases(cases_path);

%% Set result parameters.
result_path = GetFullPath(fullfile(pwd(), '..', 'results'));
mkdir(result_path);
figure_format = 'png';

%% Perform computations for all cases.
for i = 1:length(cases)
    disp(cases{i}.foldername);
    plot_name_prefix = [cases{i}.foldername ': '];
    %% Read pre- and postoperative points.
    points_input_pre = read_Osirix_points(cases{i}.filepath_pre);
    points_input_post = read_Osirix_points(cases{i}.filepath_post);
    
    %% Determine global normal of postoperative points.
    view_direction = points_normal(points_input_post);
    % Plot.
    p = plot_pair([plot_name_prefix 'Input Points'], ...
        points_input_pre, 'Preoperative', ...
        points_input_post, 'Postoperative', ...
        @plot3m, plot3style, view_direction);
    save_figure(p, result_path, figure_format);
    
    %% Determine diameter.
    % Use largest pairwise distance between all input points.
    cases{i}.diameter_pre = max(max(pdist(points_input_pre)));
    cases{i}.diameter_post = max(max(pdist(points_input_post)));
    
    %% Compute ordering of points along a curve.
    points_ordered_pre  = minimal_hamiltonian_cycle(points_input_pre);
    points_ordered_post = minimal_hamiltonian_cycle(points_input_post);
    % Plot.
    p = plot_pair([plot_name_prefix 'Ordered Points'], ...
        points_ordered_pre, 'Preoperative', ...
        points_ordered_post, 'Postoperative', ...
        @plot3m, plot3style, view_direction);
    save_figure(p, result_path, figure_format);
    
    %% Resample points along spline curves.
    spline_pre  = cscvn(points_ordered_pre([1:end 1], :)');
    spline_post = cscvn(points_ordered_post([1:end 1], :)');
    % Plot.
    p = plot_pair([plot_name_prefix 'Interpolating Spline Function'], ...
        spline_pre, 'Preoperative', spline_pre, 'Postoperative', ...
        @fnplt_options, {}, view_direction);
    save_figure(p, result_path, figure_format);
    % Resample.
    new_count = 1000;
    new_breaks_pre  = spline_pre.breaks(end) * (0:new_count-1) / new_count;
    new_breaks_post = spline_post.breaks(end) * (0:new_count-1) / new_count;
    points_pre  = fnval(spline_pre, new_breaks_pre)';
    points_post = fnval(spline_post, new_breaks_post)';
    % Plot.
    p = plot_pair([plot_name_prefix 'Resampled Points'], ...
        points_pre, 'Preoperative', points_post, 'Postoperative', ...
        @plot3m, plot3style, view_direction);
    save_figure(p, result_path, figure_format);
    
    %% Register resampled point clouds.
    pcloud_pre  = pointCloud(points_pre);
    pcloud_post = pointCloud(points_post);
    if REGISTER_CURVES
        [offset_origin, offset_transform, pcloud_offset] = centered_registration( ...
            pcloud_post, pcloud_pre, ...
            'MaxIterations', 1000, 'Tolerance', [0.001, 0.001], 'Verbose', false);
        % Plot.
        p = plot_pair([plot_name_prefix 'Registered Points'], ...
            {pcloud_pre, pcloud_post}, 'Input', ...
            {pcloud_pre, pcloud_offset}, 'Registered', ...
            @pcshowpair, {}, view_direction);
        save_figure(p, result_path, figure_format);
    end
    
    %% Compute per-point distances.
    if REGISTER_CURVES
        points_offset = pcloud_offset.Location;
        pairwise_distances = pdist2(points_pre, points_offset);
        pre_for_offset_distances = min(pairwise_distances, [], 1);
    else
        pairwise_distances = pdist2(points_pre, points_post);
        pre_for_post_distances = min(pairwise_distances, [], 1);
    end
    
    % Plot.
    p = figure('Name', [plot_name_prefix 'Distance Histogram']);
    
    if REGISTER_CURVES
        histogram(pre_for_offset_distances, 20);
        title('Distribution for nearest preoperative point for every offset point');
    else
        histogram(pre_for_post_distances, 20);
        title('Distribution for nearest preoperative point for every postoperative point');
    end
    save_figure(p, result_path, figure_format);
    
    %% Store properties.
    cases{i}.view_direction = view_direction;
    
    %% Store point clouds.
    cases{i}.pcloud_pre    = pcloud_pre;
    cases{i}.pcloud_post   = pcloud_post;
    if REGISTER_CURVES
        cases{i}.pcloud_offset = pcloud_offset;
    end
    
    %% Report differences.
    if REGISTER_CURVES
        cases{i}.global_translation       = offset_transform.T(4, 1:3);
        cases{i}.global_distance          = norm(cases{i}.global_translation);
        cases{i}.local_distance_average   = mean(pre_for_offset_distances);
        cases{i}.local_distance_deviation = std(pre_for_offset_distances);
    else
        cases{i}.local_distance_average   = mean(pre_for_post_distances);
        cases{i}.local_distance_deviation = std(pre_for_post_distances);
    end
end

%% Generate summary.
if REGISTER_CURVES
    offsets = cellfun(@(c) c.global_distance, cases);
end
averages = cellfun(@(c) c.local_distance_average, cases);
deviations = cellfun(@(c) c.local_distance_deviation, cases);
close all;
h = figure('Name', 'Curve Distances Overview');
%yyaxis left;
%p = plot(offsets, '--', 'Color', 0.3*[1 1 1]);
%p.Parent.YColor = 0.5*[1 1 1];
%ylabel(p.Parent, 'Distance [mm]');
%yyaxis right;
p = errorbar(averages, deviations, 'o', 'Color', 0*[1 1 1], ...
    'MarkerFaceColor', 'auto');
xlabel(p.Parent, 'Case');
xlim(p.Parent, [1-0.5 length(cases)+0.5]);
p.Parent.YColor = 0*[1 1 1];
ylabel(p.Parent, 'Distance [mm]');
%l = legend( ... %'Curve diameter',
%    'Global offset', 'Mean local distance', ...
%    'Location', 'NorthOutside', 'Orientation', 'Horizontal');
%title(l, 'Per-case results')
saveas(h, fullfile(result_path, 'curve-distance-overview.png'));

%% Generate summary plot.
close all;
h = figure('Name', 'Registered Outlines Overview');
set(h, 'Position', [100 100 1600 600]);
for i = 1:length(cases)
    hsub = subplot(2,5,i);
    if REGISTER_CURVES
        pcshowpairbw(cases{i}.pcloud_pre, cases{i}.pcloud_offset);
    else
        pcshowpairbw(cases{i}.pcloud_pre, cases{i}.pcloud_post);
    end
    view(hsub, cases{i}.view_direction);
end
saveas(h, fullfile(result_path, 'outline-overview.png'));

%% Write summary results.
Cases = cellfun(@(c) c.foldername, cases, 'UniformOutput', false)';
Mean = cellfun(@(c) c.local_distance_average, cases)';
Std = cellfun(@(c) c.local_distance_deviation, cases)';
DiameterPre = cellfun(@(c) c.diameter_pre, cases)';
DiameterPost = cellfun(@(c) c.diameter_post, cases)';
T = table(Mean, Std, DiameterPre, DiameterPost, 'RowNames', Cases);
writetable(T, fullfile(result_path, 'results.txt'), ...
    'WriteRowNames', true, 'Delimiter', ';');

disp('FINISHED')
