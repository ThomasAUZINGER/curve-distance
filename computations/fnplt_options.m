function [ h ] = fnplt_options( spline, varargin )
%FNPLTAX Version of fnplt that supports surface properties
%   Detailed explanation goes here

points = fnplt(spline);
h = plot3m(points, varargin{:});

end

