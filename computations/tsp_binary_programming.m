function [ adjacency ] = tsp_binary_programming( distances )
%BINARY_PROGRAMMING Computes the shortest cycle given the pairwise
% distances in a graph.
%   See http://de.mathworks.com/help/optim/ug/travelling-salesman-problem.html
%% Check input.
p = inputParser;
addRequired(p, 'distances', @(x) validateattributes(x, {'numeric'}, ...
    {'2d', 'square', 'nonnegative'}));
parse(p, distances);
if ~issymmetric(distances)
    error('The value of ''distances'' is invalid. Expected input to be symmetric.');
end

% Check for connectedness.
adjacency_input = isfinite(distances);
adjacency_input(logical(eye(size(adjacency_input)))) = 0; % Remove self loops.
G = graph(adjacency_input);
if length(conncomp(G, 'OutputForm', 'cell')) > 1
    error('The graph given by the distanced is not connected.')
end

%% Compute valid edges.
nStops = size(distances, 1);
idxs = nchoosek(1:nStops, 2);
dist = distances(sub2ind(size(distances), idxs(:,1), idxs(:,2)));
idxs(~isfinite(dist), :) = [];
dist(~isfinite(dist)) = [];
lendist = length(dist);

%% Generate constraints.
% Cycle length constraint.
Aeq = spones(1:length(idxs));
beq = nStops;
% Vertex degree constraint.
Aeq = [Aeq; spalloc(nStops, length(idxs), nStops*(nStops-1))];
for ii = 1:nStops
    whichIdxs = (idxs == ii); % find the trips that include stop ii
    whichIdxs = sparse(sum(whichIdxs,2)); % include trips where ii is at either end
    Aeq(ii+1,:) = whichIdxs'; % include in the constraint matrix
end
beq = [beq; 2*ones(nStops,1)];
% Set up binary decision variables.
intcon = 1:lendist;
lb = zeros(lendist,1);
ub = ones(lendist,1);

%% Solve binary integer program.
opts = optimoptions('intlinprog', 'Display', 'off');
[x_tsp, ~, exitflag, output] = ...
    intlinprog(dist, intcon, [], [], Aeq, beq, lb, ub, opts);

%% Iteratively remove subcycles.
tours = detectSubtours(x_tsp,idxs);
numtours = length(tours); % number of subtours
A = spalloc(0,lendist,0); % Allocate a sparse linear inequality constraint matrix
b = [];
while numtours > 1 % repeat until there is just one subtour
    fprintf('Binary integer programming: %d subcycles found - further iteration necessary.\n',numtours);
    % Add the subtour constraints
    b = [b; zeros(numtours, 1)]; % allocate b
    A = [A; spalloc(numtours, lendist, nStops)]; % a guess at how many nonzeros to allocate
    for ii = 1:numtours
        rowIdx = size(A, 1) + 1; % Counter for indexing
        subTourIdx = tours{ii}; % Extract the current subtour
%         The next lines find all of the variables associated with the
%         particular subtour, then add an inequality constraint to prohibit
%         that subtour and all subtours that use those stops.
        variations = nchoosek(1:length(subTourIdx), 2);
        for jj = 1:length(variations)
            whichVar = (sum(idxs == subTourIdx(variations(jj, 1)), 2)) & ...
                       (sum(idxs == subTourIdx(variations(jj, 2)), 2));
            A(rowIdx, whichVar) = 1;
        end
        b(rowIdx) = length(subTourIdx) - 1; % One less trip than subtour stops
    end

    % Try to optimize again
    [x_tsp, ~, exitflag, output] = intlinprog(dist, intcon, ...
        A, b, Aeq, beq, lb, ub, opts);

    % How many subtours this time?
    tours = detectSubtours(x_tsp,idxs);
    numtours = length(tours); % number of subtours
end

%% Check solution.
if exitflag ~= 1 || output.absolutegap > 0
    error('None or invalid solution found.');
end
x_tsp = logical(round(x_tsp)); % All variables binary.

%% Report adjacency matrix.
adjacency = false(size(distances));
indices = sub2ind(size(adjacency), idxs(x_tsp, 1), idxs(x_tsp, 2));
adjacency(indices) = true;
adjacency = adjacency | adjacency'; % Symmetrize adjacency matrix.

end
