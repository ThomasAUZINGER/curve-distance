function [ h ] = plot_pair( name, data1, title1, data2, title2, ...
    command, options, view_dir )
%PLOT_PAIR Plots a pair of identical plots with different underlying data
%and links their view transforms.
%   Detailed explanation goes here

% Check if view direction supplied.
if nargin >= 8
    set_direction = true;
else
    set_direction = false;
end
% Generate named figure.
h = figure('Name', name);
set(h, 'Position', get(h, 'Position') .* [1 1 2 1]); % Double figure width.
% Plot.
hsub(1) = subplot(121);
if iscell(data1)
    command(data1{:}, options{:}, 'Parent', hsub(1));
    if set_direction, view(hsub(1), view_dir); end
else
    command(data1, options{:}, 'Parent', hsub(1));
    if set_direction, view(hsub(1), view_dir); end
end
title(title1);
hsub(2) = subplot(122);
if iscell(data2)
    command(data2{:}, options{:}, 'Parent', hsub(2));
    if set_direction, view(hsub(2), view_dir); end
else
    command(data2, options{:}, 'Parent', hsub(2));
    if set_direction, view(hsub(2), view_dir); end
end
title(title2);
% Link plots and store link (see
% http://de.mathworks.com/matlabcentral/answers/12171#answer_16831 )
setappdata(h, 'Link', linkprop(hsub, 'CameraPosition'));

end
