function [ ] = save_figure( figure, path, format )
%SAVE_FIGURE Formats and saves a figure to file.
%   Detailed explanation goes here

filename = matlab.lang.makeValidName(get(figure, 'Name'));
filepath = fullfile(path, [filename '.' format]);
saveas(figure, filepath);

end

