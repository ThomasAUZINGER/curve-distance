function [ h ] = surfm( points, varargin )
%SURFM Surf for array inputs.
%   Detailed explanation goes here

temp = num2cell(points, 2);
[X, Y, Z] = temp{:}; % Extract columns.
h = surf(X, Y, Z, varargin{:});

end

