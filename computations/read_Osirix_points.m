function [ points ] = read_Osirix_points( filepath )
%READ_OSIRIX_POINTS Reads points from Osirix generated file.
%   Detailed explanation goes here

% Osirix inserts ',...' after the last column header. This needs to be
%  removed using the method provided in the answer to
%  https://de.mathworks.com/matlabcentral/answers/253238
fileID = fopen(filepath, 'r');
line = fgetl(fileID);
fclose(fileID);
vars = regexp(line, ',', 'split');
if strcmp(vars{end}, '...')
  vars = vars(1:end-1);
end
% Read the data into a table and insert the variable names.
data = readtable(filepath, 'HeaderLines', 1, 'ReadVariableNames', false);
data.Properties.VariableNames = vars;
% Extract the points.
column_names = {'mmX', 'mmY', 'mmZ'};
points = table2array(data(:, column_names));

end

