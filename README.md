# README

## Curve Distance Computation

Compute the distance between two closed curves in three dimensions.

### Requirements

- `MATLAB`
- Input data has to conform to the following format:
  - Each input case has its own folder starting with `Case`, which is stored in a root folder `./data/` (e.g., `./data/Case XYZ/`).
  - Each input case has to contain two data files whose filenames end with `pre.csv` and `post.csv` (e.g., `ABC pre.csv` and `ABC post.csv`).<br>
  These files contain the pre- and postoperative coordinates of the curves in `Osirix` format.<br>In essence, a table is expected that can be read with `readtable` and contains the column header `mmX`, `mmY`, and `mZ` that contain the coordinates.

### Instructions

1. Open `./computations/main.m` in `MATLAB`.
2. Choose if the distances between the curves should be reported for the original input curves (by setting `REGISTER_CURVES = false;`) or for rigidly aligned curves (by setting `REGISTER_CURVES = false;`).
2. The output can be found in the folder `./results/`.

